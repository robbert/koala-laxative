#[macro_use]
extern crate serde_derive;

extern crate clap;
extern crate hyper;
extern crate hyper_tls;
extern crate rand;
extern crate serde_json;
extern crate tokio;
extern crate tokio_timer;

use diff::Activity;
use error::Error;

use clap::{App, Arg};
use hyper::client::connect::Connect;
use hyper::{header, Body, Client, Request};
use hyper_tls::HttpsConnector;
use rand::Rng;
use std::sync::{Arc, RwLock};
use std::time::{Duration, Instant};
use tokio::prelude::*;
use tokio_timer::{Delay, Interval};

pub mod diff;
pub mod error;
pub mod telegram;

const API_ENDPOINT: &str = "https://koala.svsticky.nl/api/activities";
const USER_AGENT: &str = "hyper/0.12.1 (github.com/robbert-vdh; ik wil gewoon notificaties)";

fn main() {
    let matches = App::new("Koala Laxative")
        .arg(
            Arg::with_name("telegram token")
                .short("t")
                .long("token")
                .help("Enables output to a Telegram channel")
                .takes_value(true)
                .requires("telegram channel"),
        )
        .arg(
            Arg::with_name("telegram channel")
                .short("c")
                .long("channel")
                .help("The Telegram channel name to use")
                .takes_value(true),
        )
        .get_matches();

    let should_use_telegram = matches.is_present("telegram token");
    let telegram_token = matches.value_of("telegram token").unwrap_or("").to_string();
    let telegram_channel = matches
        .value_of("telegram channel")
        .unwrap_or("")
        .to_string();
    if should_use_telegram {
        println!("Outputting to Telegram channel '@{}'\n", telegram_channel);
    }

    // TODO: Migrate from hyper to reqwest when their async support stabilizes
    let https_connector = HttpsConnector::new(4).unwrap();
    let client = Client::builder().build(https_connector);
    let telegram_client = client.clone();

    // TODO: Instead of just comparing and showing *any* change in activities, we should instead
    //       look for differences between the old and the new data so we can.
    let last_activities = Arc::new(RwLock::new(Vec::new()));

    // We have to fetch the current activities first, as we don't have any persistence yet
    let initial_fetch = {
        let last_activities = last_activities.clone();

        retrieve_activities(&client)
            .map(move |activities: Vec<Activity>| {
                *last_activities.write().unwrap() = activities;
            })
            .map_err(|error| panic!("Error occured during initial fetch: {}", error))
    };

    let handle_updates = delayed_interval(10_000, 0, 5_000)
        .then(move |_| retrieve_activities(&client))
        .for_each(move |activities: Vec<Activity>| {
            if activities != *last_activities.read().unwrap() {
                if should_use_telegram {
                    let task =
                        telegram::send_message(
                            &telegram_client,
                            &telegram_token,
                            &telegram_channel,
                            format!("New Update!\n\n{:#?}", activities),
                        ).map_err(|error| eprintln!("Could not output to telegram: {}", error));

                    tokio::spawn(task);
                } else {
                    println!("{:#?}", activities);
                }

                *last_activities.write().unwrap() = activities;
            }

            Ok(())
        })
        .map_err(|error| panic!("Error occured while fetching: {}", error));

    tokio::run(initial_fetch.and_then(|_| handle_updates));
}

/// Create a stream that will emit the current time every `interval_ms` miliseconds. Every item of
/// this stram is then delayed by a random number of miliseconds between `delay_lower_ms` and
/// `delay_upper_ms`. This is mostly useful to reduce the load on the Koala server.
fn delayed_interval(
    interval_ms: u64,
    delay_lower_ms: u64,
    delay_upper_ms: u64,
) -> impl Stream<Item = (), Error = tokio_timer::Error> {
    let starting_time = Instant::now();

    Interval::new(starting_time, Duration::from_millis(interval_ms)).and_then(move |time| {
        // Don't delay the first request, mostly for debug purposes
        if time == starting_time {
            Delay::new(time)
        } else {
            let delay_ms = rand::thread_rng().gen_range(delay_lower_ms, delay_upper_ms);

            Delay::new(time + Duration::from_millis(delay_ms))
        }
    })
}

/// Retrieve and deserialize the current list of activities.
fn retrieve_activities<C: Connect + 'static>(
    client: &Client<C>,
) -> impl Future<Item = Vec<Activity>, Error = Error> {
    client
        .request(
            Request::get(API_ENDPOINT)
                .header(header::USER_AGENT, USER_AGENT)
                .body(Body::empty())
                .unwrap(),
        )
        .and_then(|response| response.into_body().concat2())
        .map_err(Error::HttpError)
        .and_then(|body| serde_json::from_slice(&body.to_vec()).map_err(Error::ParseError))
}
