use hyper;
use serde_json;
use std::fmt;
use std::fmt::{Display, Formatter};

/// Combined error type for hyper and serde errors.
#[derive(Debug)]
pub enum Error {
    HttpError(hyper::Error),
    ParseError(serde_json::Error),
    /// This variant contains the JSON returned by Telegram's API.
    TelegramError(String),
}

impl Display for Error {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        match self {
            Error::HttpError(e) => e.fmt(f),
            Error::ParseError(e) => e.fmt(f),
            Error::TelegramError(e) => e.fmt(f),
        }
    }
}
