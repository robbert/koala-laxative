//! This module contains a tiny wrapper around Telegram's API to send messages to a channel.

use error::Error;

use hyper::client::connect::Connect;
use hyper::{header, Client, Request};
use serde_json;
use tokio::prelude::*;

/// Macro for formatting API URLs as Rust doesn't allow constants in `format!` macros.
macro_rules! api_url {
    ($token:expr, $method:expr) => {
        format!(
            "https://api.telegram.org/bot{token}/{method}",
            token = $token,
            method = $method
        )
    };
}

/// Sends a message to channel `channel` containing `message`. Will return Telegram's own response
/// when the request is succesful.
pub fn send_message<C: Connect + 'static>(
    client: &Client<C>,
    api_token: &str,
    channel: &str,
    message: String,
) -> impl Future<Item = (), Error = Error> {
    let body_json = serde_json::to_string(&SendMessageBody {
        chat_id: format!("@{}", channel),
        text: message,
    }).unwrap();

    let request = Request::post(api_url!(api_token, "sendMessage"))
        .header(header::CONTENT_TYPE, "application/json")
        .body(body_json.into())
        .unwrap();

    client
        .request(request)
        .map_err(Error::HttpError)
        .and_then(|response| {
            // TODO: Think of a way to do this that looks a little less intimidating
            // When the request fails, we want to log Telegram's JSON error, but we're not
            // interested in the actual result itself
            let is_succes = response.status().is_success();

            response
                .into_body()
                .concat2()
                .map_err(Error::HttpError)
                .map(|body| String::from_utf8(body.to_vec()).unwrap())
                .and_then(move |json| {
                    if is_succes {
                        Ok(())
                    } else {
                        Err(Error::TelegramError(json))
                    }
                })
        })
}

/// The request body for `https://core.telegram.org/bots/api#sendmessage`.
#[derive(Serialize)]
struct SendMessageBody {
    /// Unique identifier for the target chat or username of the target channel (in the format
    /// `@channelusername`).
    pub chat_id: String,
    /// Text of the message to be sent.
    pub text: String,
}
