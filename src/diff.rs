//! This module contains a function for finding the differences between two lists of activities.
//!
//! Since these activities don't come with a key, we 'll try two methods to find matching
//! activities:
//!
//! - Using the activity's name
//! - Using the activity's start date (in case the name changed)

use std::collections::{HashMap, HashSet};

#[derive(Deserialize, Debug, PartialEq, Eq)]
pub struct Activity {
    pub name: String,
    pub location: String,
    pub start_date: String,
    pub end_date: String,
    pub poster: String,
}

#[derive(Debug)]
pub enum Difference {
    Added(Activity),
    Removed(Activity),

    ChangedName(Activity, String),
    ChangedLocation(Activity, String),
    ChangedStart(Activity, String),
    ChangedEnd(Activity, String),
    ChangedPoster(Activity, String),
}

pub fn find_differences(
    old_activities: &[Activity],
    new_activities: &[Activity],
) -> Vec<Difference> {
    // The current method does not support multiple activities with the same name yet
    assert!(no_duplicate_names(old_activities));
    assert!(no_duplicate_names(new_activities));

    let mut old_by_name: HashMap<&str, &Activity> = old_activities
        .iter()
        .map(|activity| (activity.name.as_str(), activity))
        .collect();
    let mut new_by_name: HashMap<&str, &Activity> = new_activities
        .iter()
        .map(|activity| (activity.name.as_str(), activity))
        .collect();

    unimplemented!();
}

/// Checks whether there are activities with duplicate names. The current method does not support
/// multiple activities with the same name.
fn no_duplicate_names(activities: &[Activity]) -> bool {
    let names: HashSet<&str> = activities
        .iter()
        .map(|activity| activity.name.as_str())
        .collect();

    names.len() == activities.len()
}
